# Homework 2
DS:BDA Homework 2 repo
### Prerequisites
- Apache Ignite
- Apache Kafka
- Apache Zookeeper
- Java 8
### How To
Build project: 
```sh
$ git clone git@bitbucket.org:levandy/homework1.git
$ cd homework1
$ mvn clean install
```
Prepare data:
```sh
$ python data.py
```
Run Zookeeper:
```sh
$ <path_to_zookeeper>/bin/zkServer.sh start
$ <path_to_zookeeper>/bin/zkCli.sh
```
Run Kafka:
```sh
$ <path_to_kafka>/bin/kafka-server-start.sh <path_to_kafka>/config/server.properties
```
Run Ignite:
```sh
$ <path_to_ignite>/bin/ignite.sh <path_to_hw2-repo>/config.xml
```
Run produce:
```sh
$ cd target
$ java -jar hw2-1.0-SNAPSHOT-with-dependencies.jar produce ../tweets_data.txt
```
Run store:
```sh
$ java -jar hw2-1.0-SNAPSHOT-with-dependencies.jar store
```
Run compute:
```sh
$ java -jar hw2-1.0-SNAPSHOT-with-dependencies.jar compute
```