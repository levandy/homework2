try:
    import json
except ImportError:
    import simplejson as json

# Import the necessary methods from "twitter" library
from twitter import Twitter, OAuth, TwitterHTTPError, TwitterStream

# Variables that contains the user credentials to access Twitter API
ACCESS_TOKEN = '873216564413255680-l6ZbKr29CnKiByuG7m9XMhY3sB3d1m8'
ACCESS_SECRET = 'jvc5yM7G4baiZJ3Jx4BkbYCXzHrtHYH0MKpFi65EeceMZ'
CONSUMER_KEY = 'ISOo4PYkk1f1eL0Z95EchMZM4'
CONSUMER_SECRET = 'O8RTukwBTROiaIWnU3iAKARMrUgcFFtn70dihpECYhvBUFn7Qc'

oauth = OAuth(ACCESS_TOKEN, ACCESS_SECRET, CONSUMER_KEY, CONSUMER_SECRET)

# Initiate the connection to Twitter Streaming API
twitter_stream = TwitterStream(auth=oauth)

# Get a sample of the public data following through Twitter
iterator = twitter_stream.statuses.sample()

# Print each tweet in the stream to the file
with open("tweets_data.txt", 'a') as file:
    tweet_count = 1000
    for tweet in iterator:
        tweet_count -= 1
        # Twitter Python Tool wraps the data returned by Twitter
        # as a TwitterDictResponse object.
        # convert it back to the JSON
        file.write(json.dumps(tweet))

        if tweet_count <= 0:
            break

print("Data generated!")
