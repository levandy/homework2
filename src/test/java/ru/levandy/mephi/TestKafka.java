package ru.levandy.mephi;


import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Test class for Kafka producer and consumer
 */
public class TestKafka {
    private KafkaConsumer<Integer, String> twitterConsumer;
    private KafkaTwitterProducer twitterProducer;

    /**
     * Init method for producer and consumer
     */
    @Before
    public void setUp() {
        // init producer
        twitterProducer = new KafkaTwitterProducer();
        twitterProducer.setProducer(new KafkaProducer<>(Main.getProducerProps()));
        twitterProducer.setTopic("testTopic");
        //set test file
        twitterProducer.setFile(getClass().getClassLoader().getResource("test.txt").getFile());
        //init consumer
        twitterConsumer = new KafkaConsumer<>(Main.getConsumerProps());
    }


    /**
     * Test method
     * @throws IOException
     */
    @Test
    public void testProducer() throws IOException {
        // start producer and close then
        twitterProducer.processData();
        twitterProducer.close();
        // subscribe consumer to topic
        twitterConsumer.subscribe(Collections.singletonList("testTopic"));
        //get records
        ConsumerRecords<Integer, String> records = twitterConsumer.poll(1000);
        Map<Integer, String> res = new HashMap<>();
        // got by consumer
        for (ConsumerRecord<Integer, String> record : records) {
            res.put(record.key(), record.value());
        }
        twitterConsumer.commitSync();
        // real datas
        Map<Integer, String> exp = new HashMap<>();
        exp.put(0, "delete;1513195691368");
        exp.put(1, "delete;1513195691643");
        exp.put(2, "favorited;1513195691661");
        Assert.assertEquals(exp, res);
    }
}
