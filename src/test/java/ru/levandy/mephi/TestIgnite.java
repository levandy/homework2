package ru.levandy.mephi;


import org.apache.ignite.*;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.Map;

/**
 * Test class for ignite
 */
public class TestIgnite {
    private Ignite ignite = null;
    private IgniteCompute compute = null;
    private IgniteCache<Integer, String> cache = null;

    /**
     * init method
     */
    @Before
    public void setUp() {
        try {
            // init ignite
            Ignition.setClientMode(true);
            ignite = Ignition.start(getClass().getClassLoader().getResource("config.xml").getFile());
            ignite.active(true);
            compute = ignite.compute();
            CacheConfiguration<Integer, String> cacheCfg = new CacheConfiguration<>("cache");
            cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
            cacheCfg.setBackups(1);
            cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC);
            cacheCfg.setIndexedTypes(Integer.class, String.class);
            // cache
            cache = ignite.getOrCreateCache(cacheCfg);
            cache.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test with one line of data
     */
    @Test
    public void testOne() {
        // write to cache
        IgniteDataStreamer<Integer, String> streamer = ignite.dataStreamer("cache");
        streamer.addData(1, "favorited;1513195690662");
        streamer.flush();
        // compute and check
        Map<TweetWrapper, Integer> result = compute.execute(ComputeIgnite.class, cache);
        Assert.assertTrue(result.containsKey(new TweetRecord(1513195690662L, "favorited").convertToWrapper()));
        Assert.assertTrue(result.get(new TweetRecord(1513195690662L, "favorited").convertToWrapper()) == 1);
    }

    /**
     * Test with two "equal" lines of data
     */
    @Test
    public void testTwoSame() {
        // write to cache
        IgniteDataStreamer<Integer, String> streamer = ignite.dataStreamer("cache");
        streamer.addData(1, "favorited;1513195690662");
        streamer.addData(2, "favorited;1513195690666");
        streamer.flush();
        // compute and check
        Map<TweetWrapper, Integer> result = compute.execute(ComputeIgnite.class, cache);
        Assert.assertTrue(result.containsKey(new TweetRecord(1513195690662L, "favorited").convertToWrapper()));
        Assert.assertTrue(result.containsKey(new TweetRecord(1513195690666L, "favorited").convertToWrapper()));
        Assert.assertTrue(result.get(new TweetRecord(1513195690662L, "favorited").convertToWrapper()) == 2);
    }

    /**
     * Test with two different lines of data
     */
    @Test
    public void testTwoDiff() {
        // write to cache
        IgniteDataStreamer<Integer, String> streamer = ignite.dataStreamer("cache");
        streamer.addData(1, "favorited;1513195690662");
        streamer.addData(2, "delete;1513195690666");
        streamer.flush();
        // compute and check
        Map<TweetWrapper, Integer> result = compute.execute(ComputeIgnite.class, cache);
        Assert.assertTrue(result.containsKey(new TweetRecord(1513195690662L, "favorited").convertToWrapper()));
        Assert.assertTrue(result.get(new TweetRecord(1513195690662L, "favorited").convertToWrapper()) == 1);
        Assert.assertTrue(result.containsKey(new TweetRecord(1513195690666L, "delete").convertToWrapper()));
        Assert.assertTrue(result.get(new TweetRecord(1513195690666L, "delete").convertToWrapper()) == 1);
    }

    /**
     * Close ignite after test
     */
    @After
    public void tearDown() {
        ignite.close();
    }
}
