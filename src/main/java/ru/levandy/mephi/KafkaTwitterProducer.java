package ru.levandy.mephi;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class KafkaTwitterProducer {
    private Producer<Integer, String> producer;
    private String topic;
    private String file;

    public Producer<Integer, String> getProducer() {
        return producer;
    }

    public void setProducer(Producer<Integer, String> producer) {
        this.producer = producer;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public KafkaTwitterProducer() {
    }

    /**
     * Constructor
     * @param topic topic where to write
     * @param file filename
     * @param properties properties
     */
    public KafkaTwitterProducer(String topic, String file, Properties properties) {
        this.producer = new KafkaProducer<>(properties);
        this.topic = topic;
        this.file = file;
    }

    /**
     * Read from file, parse data as JSON and send to producer
     */
    public void processData(){
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            int counter = 0;
            System.out.println("Started process");
            while ((line = br.readLine()) != null) {
                // get JSON
                org.json.JSONObject obj = new JSONObject(line);
                if (obj.has("delete")) {
                    // create new record with delete (counter, delete;timestamp) and send it
                    ProducerRecord<Integer, String> record = new ProducerRecord<>(topic, counter, "delete;"+ obj.getJSONObject("delete").getString("timestamp_ms"));
                    producer.send(record);
                } else if (obj.has("favorited")) {
                    // create new record with favorited (counter, favorited;timestamp) and send it
                    ProducerRecord<Integer, String> record = new ProducerRecord<>(topic, counter, "favorited;"+obj.getString("timestamp_ms"));
                    producer.send(record);
                }
                counter ++;
            }

            System.out.println("Finished process");
        } catch (IOException e) {
            e.printStackTrace();
        }
//        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
//        configurationBuilder
//                .setOAuthConsumerKey("ev5NqQ8viO31uGUul3RXIbfPR")
//                .setOAuthConsumerSecret("xFU0oEX24bgHKTfYasmnXUFLrtq5MXx4e8DXnGVsyGwAklcZWO")
//                .setOAuthAccessToken("873216564413255680-l6ZbKr29CnKiByuG7m9XMhY3sB3d1m8")
//                .setOAuthAccessTokenSecret("jvc5yM7G4baiZJ3Jx4BkbYCXzHrtHYH0MKpFi65EeceMZ");
//        TwitterStream stream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();
//        stream.addListener(new StatusListener() {
//            @Override
//            public void onStatus(Status status) {
//                System.out.println(status.getCreatedAt().toString());
//            }
//
//            @Override
//            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
//
//            }
//
//            @Override
//            public void onTrackLimitationNotice(int i) {
//
//            }
//
//            @Override
//            public void onScrubGeo(long l, long l1) {
//
//            }
//
//            @Override
//            public void onStallWarning(StallWarning stallWarning) {
//
//            }
//
//            @Override
//            public void onException(Exception e) {
//
//            }
//        });
//        while (true) {
//            stream.sample();
//        }
    }

    public void close() {
        this.producer.close();
    }
}
