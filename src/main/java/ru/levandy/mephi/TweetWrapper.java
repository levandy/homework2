package ru.levandy.mephi;

/**
 * Wrapper class for grouping of data and calculating it in ignite
 */
public class TweetWrapper {
    // time of tweet
    private int hour;
    private int minute;
    private int second;
    // key word
    private String word;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    /**
     * Constructor
     * @param hour hours
     * @param minute minutes
     * @param second seconds
     * @param word key word
     */
    public TweetWrapper(int hour, int minute, int second, String word) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.word = word;
    }

    @Override
    public String toString() {
        return  word + " --> " + hour + ":" + minute + ":" + second;
    }

    @Override
    public boolean equals(Object o) {
        TweetWrapper tweetPair = (TweetWrapper) o;

        if (hour != tweetPair.hour) return false;
        if (minute != tweetPair.minute) return false;
        if (second != tweetPair.second) return false;
        return word != null ? word.equals(tweetPair.word) : tweetPair.word == null;
    }

    /**
     * Group data by hashCode
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return hour*100000 + minute * 10000 + second * 100 + (word.equalsIgnoreCase("delete")?1:2);
    }
}
