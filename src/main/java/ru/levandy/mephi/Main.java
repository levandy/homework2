package ru.levandy.mephi;

import org.apache.ignite.*;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import java.util.*;

/**
 * Main class
 * java -jar hw1...-with-dependencies.jar produce file
 * java -jar hw1...-with-dependencies.jar store
 * java -jar hw1...-with-dependencies.jar compute
 */
public class Main {
    private final static String BOOTSTRAP_SERVERS = "localhost:9092";
    private final static String STRING_SERIALIZER = "org.apache.kafka.common.serialization.StringSerializer";
    private final static String STRING_DESERIALIZER = "org.apache.kafka.common.serialization.StringDeserializer";
    private final static String INTEGER_SERIALIZER = "org.apache.kafka.common.serialization.IntegerSerializer";
    private final static String INTEGER_DESERIALIZER = "org.apache.kafka.common.serialization.IntegerDeserializer";

    public static void main(String[] args){
        // check params
        if (args.length > 0) {
            // produce from file
            if (args.length == 2 && "produce".equalsIgnoreCase(args[0])) {
                // get filename
                String filename = args[1];
                // init producer
                KafkaTwitterProducer producer = new KafkaTwitterProducer("topic", filename, getProducerProps());
                // process adn close
                producer.processData();
                producer.close();
            } else if (args.length == 1) {
                // init and start ignite
                Ignition.setClientMode(true);
                Ignite ignite = Ignition.start("/home/andyleo/homework2/config.xml");
                ignite.active(true);
                CacheConfiguration<Integer, String> cacheCfg = new CacheConfiguration<>("myCache");
                cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
                cacheCfg.setBackups(1);
                cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC);
                cacheCfg.setIndexedTypes(Integer.class, String.class);
                // get cache
                IgniteCache<Integer, String> cache = ignite.getOrCreateCache(cacheCfg);
                if ("store".equalsIgnoreCase(args[0])) {
                    // create streamer
                    IgniteDataStreamer<Integer, String> streamer = ignite.dataStreamer("myCache");
                    // get consumer
                    KafkaConsumer<Integer, String> consumer = new KafkaConsumer<>(getConsumerProps());
                    // subscribe to topic and get records
                    consumer.subscribe(Collections.singletonList("topic"));
                    ConsumerRecords<Integer, String> records = consumer.poll(1000);
                    // put to streamer
                    for (ConsumerRecord<Integer, String> record : records) {
                        streamer.addData(record.key(), record.value());
                    }
                    consumer.commitSync();
                    streamer.flush();
                } else if ("compute".equalsIgnoreCase(args[0])) {
                    if (cache.size() != 0) {
                        // init computation
                        IgniteCompute compute = ignite.compute();
                        // execute computation
                        Map<TweetWrapper, Integer> result = compute.execute(ComputeIgnite.class, cache);
                        // print result
                        for (Map.Entry<TweetWrapper, Integer> entry : result.entrySet()) {
                            System.out.println(entry.getKey().toString() + " - " + entry.getValue());
                        }
                    } else {
                        System.out.println("Empty cache");
                    }
                } else {
                    System.out.println("Wrong params!");
                }
            } else {
                System.out.println("Wrong params!");
            }
        } else {
            System.out.println("Wrong params!");
        }
    }

    /**
     * Get properties for Kafka Producer
     * @return Properties object
     */
    static public Properties getProducerProps(){
        Properties props = new Properties();
        props.put("bootstrap.servers", BOOTSTRAP_SERVERS);
        props.put("key.serializer", INTEGER_SERIALIZER);
        props.put("value.serializer", STRING_SERIALIZER);
        return props;
    }

    /**
     * Get properties for Kafka Consumer
     * @return Properties object
     */
    static public Properties getConsumerProps(){
        Properties props = new Properties();
        props.put("bootstrap.servers", BOOTSTRAP_SERVERS);
        props.put("key.deserializer", INTEGER_DESERIALIZER);
        props.put("value.deserializer", STRING_DESERIALIZER);
        props.put("group.id", "group");
        return props;
    }
}
