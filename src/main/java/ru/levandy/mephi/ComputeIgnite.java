package ru.levandy.mephi;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteException;
import org.apache.ignite.compute.ComputeJob;
import org.apache.ignite.compute.ComputeJobAdapter;
import org.apache.ignite.compute.ComputeJobResult;
import org.apache.ignite.compute.ComputeTaskSplitAdapter;
import org.jetbrains.annotations.Nullable;

import javax.cache.Cache;
import java.util.*;


public class ComputeIgnite extends ComputeTaskSplitAdapter<IgniteCache<Integer, String>, Map<TweetWrapper,Integer>> {
    /**
     * Convert data to wrapper
     * @param i num of split
     * @param entries data from cache
     * @return jobs to compute
     * @throws IgniteException
     */
    @Override
    protected Collection<? extends ComputeJob> split(int i, IgniteCache<Integer, String> entries) throws IgniteException {
        List<ComputeJob> jobs = new ArrayList<>(entries.size());
        for (Cache.Entry<Integer, String> entry : entries) {
            jobs.add(new ComputeJobAdapter() {
                @Override
                public Object execute() throws IgniteException {
                    //get word
                    String word = entry.getValue().split(";")[0];
                    //get time
                    Long time = Long.parseLong(entry.getValue().split(";")[1]);
                    //new twitter record
                    TweetRecord rec = new TweetRecord(time, word);
                    //return wrapper
                    return rec.convertToWrapper();
                }
            });
        }
        return jobs;
    }

    /**
     * Get Hashmap with unique hour-min-sec-word and count of it
     * @param list jobs to compute
     * @return hashmap
     * @throws IgniteException
     */
    @Nullable
    @Override
    public Map<TweetWrapper, Integer> reduce(List<ComputeJobResult> list) throws IgniteException {
        Map<TweetWrapper, Integer> stat = new HashMap<>();
        for (ComputeJobResult result : list) {
            TweetWrapper wrapper = result.getData();
            Integer count = stat.get(wrapper);
            // if key doesnt exist -> count = 0
            if (count == null) {
                count = 0;
            }
            // else count ++
            stat.put(wrapper, count + 1);
        }
        return stat;
    }
}
