package ru.levandy.mephi;


import java.util.Calendar;

/**
 * Class for one tweet record from file
 */
public class TweetRecord {
    // timestamp in ms
    private long timeMs;
    // key word
    private String word;

    /**
     * Constructor
     * @param timeMs timestamp
     * @param word word
     */
    public TweetRecord(long timeMs, String word) {
        this.timeMs = timeMs;
        this.word = word;
    }

    public long getTimeMs() {
        return timeMs;
    }

    public void setTimeMs(long timeMs) {
        this.timeMs = timeMs;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    /**
     * Convert record to wrapper
     * @return new Wrapper
     */
    public TweetWrapper convertToWrapper() {
        // get calendar and set time
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMs);
        // get parts
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int mins = calendar.get(Calendar.MINUTE);
        int secs = calendar.get(Calendar.SECOND);
        return new TweetWrapper(hours, mins, secs, word);
    }
}
